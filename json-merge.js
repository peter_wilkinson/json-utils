'use strict';

var OBJECT_CONSTRUCTOR = {}.constructor;
function merge(original, patch, patchCloned) {
    var key, patchValue;
    if (!original || original.constructor !== OBJECT_CONSTRUCTOR || !patch || patch.constructor !== OBJECT_CONSTRUCTOR) {
        if (patch && patch.constructor === OBJECT_CONSTRUCTOR) {
            if (!patchCloned) {
                patch = JSON.parse(JSON.stringify(patch)); // clone patch
            }
            for (key in patch) {
                //noinspection JSUnfilteredForInLoop
                patchValue = patch[key];
                if (patchValue === null) {
                    //noinspection JSUnfilteredForInLoop
                    delete patch[key];
                }
                else if (patchValue && patchValue.constructor === OBJECT_CONSTRUCTOR) {
                    //noinspection JSUnfilteredForInLoop
                    patch[key] = merge(null, patchValue, true); // Strip any nulls from sub tree.
                }
            }
        }
        return patch;
    }

    var result = {};
    var deletedKeys = {};
	var isModified = false;
    for (key in patch) {
        //noinspection JSUnfilteredForInLoop
        patchValue = patch[key];
        if (patchValue === null) {
            //noinspection JSUnfilteredForInLoop
			isModified = deletedKeys[key] = true;
        }
        else {
			var originalValue = original[key];
			var mergeValue = merge(originalValue, patchValue);
			//noinspection JSUnfilteredForInLoop
			if (originalValue !== mergeValue) {
				result[key] = mergeValue;
				isModified = true;
			}
        }
    }

	if (!isModified) {
		return original;
	}

    for (key in original) {
        //noinspection JSUnfilteredForInLoop
        if (!deletedKeys[key] && !result.hasOwnProperty(key)) {
            //noinspection JSUnfilteredForInLoop
            result[key] = original[key];
        }
    }
    //Object.freeze(result); // Possibly freeze the object here.
    return result;
}

function generatePatch(source, target) {
    if (target === undefined || target === null) {
        return null;
    }

    if (source.constructor === OBJECT_CONSTRUCTOR && target.constructor === OBJECT_CONSTRUCTOR) {
        var patch = {};
        for (var key in source) {
            var sourceValue = source[key];
            var targetValue = target[key];
            if (sourceValue !== targetValue) {
                var keyPatch = generatePatch(sourceValue, targetValue);
                if (keyPatch && keyPatch.constructor === OBJECT_CONSTRUCTOR) {
                    for (var keyCheck in keyPatch) {
                        patch[key] = keyPatch;
                        break;
                    }
                }
                else {
                    patch[key] = keyPatch;
                }
            }
        }
        for (var key in target) {
            if (source[key] === undefined) {
                patch[key] = target[key];
            }
        }
        return patch;
    }
    return target;
}

module.exports = {
    merge : merge,
    generatePatch: generatePatch
};