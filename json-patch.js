'use strict';

var fastJsonPatch = require('fast-json-patch');
var deepCopy = require('deepcopy');

function patch(object, patches) {
	var copy = deepCopy(object);
	fastJsonPatch.apply(copy, patches);
	return copy;
}

module.exports = {
	patch: patch,
	generatePatch: fastJsonPatch.compare
};
