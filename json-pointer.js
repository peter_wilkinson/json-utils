'use strict';

var OBJECT_CONSTRUCTOR = {}.constructor;

function has(object, pointer) {
	return get(object, pointer) !== undefined;
}

function get(object, pointer) {
	var parts = pointer.split('/');
	for (var i=1, len = parts.length; object && i < len; i++) {
		object = object[parts[i]];
	}
	return object;
}

function set(object, pointer, value) {
	var parts = pointer.split('/');
	var result = {'': object};
	var workingObject = result;
	for (var i=1, iLen = parts.length; object && i < iLen; i++) {
		var newObject = {};
		var targetKey = parts[i - 1];
		var targetObject = workingObject[targetKey];
		if (!targetObject || targetObject.constructor !== OBJECT_CONSTRUCTOR) {
			targetObject = {};
		}
		for (var j=0, copyKeys = Object.keys(targetObject), jLen = copyKeys.length; j < jLen; j++) {
			var copyKey = copyKeys[j];
			//if (copyKey !== key) {
				newObject[copyKey] = targetObject[copyKey];
			//}
		}
		workingObject = workingObject[targetKey] = newObject;
		//var key = parts[i];
		//workingObject = workingObject[key] = workingObject[key] || {};
	}
	var lastPart = parts.pop();
	workingObject[lastPart] = value;
	return result[''];
}

module.exports = {
	has: has,
	get: get,
	set: set
};