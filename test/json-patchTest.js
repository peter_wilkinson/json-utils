'use strict';

var JsonPatch = require('../json-patch');
var expect = require('chai').expect;

//https://tools.ietf.org/html/rfc6902#appendix-A
describe("Json Patch ", function () {
	it('Adds an object member', function () {
		var object = {foo: 'bar'};
		var patches = [
			{"op": "add", "path": "/baz", "value": "qux"}
		];
		var actual = JsonPatch.patch(object, patches);
		expect(actual).to.deep.equal({
				"baz": "qux",
				"foo": "bar"
			});

		expect(actual).to.not.equal(object);
	});

	it('Adds an Array Element', function () {
		var object = {"foo": ["bar", "baz"]};
		var patches = [
			{"op": "add", "path": "/foo/1", "value": "qux"}
		];
		var actual = JsonPatch.patch(object, patches);
		expect(actual).to.deep.equal({"foo": ["bar", "qux", "baz"]});
		expect(actual).to.not.equal(object);
		expect(actual.foo).to.not.equal(object.foo);
	});

	it('Removes an Object Member', function () {
		expect(JsonPatch.patch(
			{
				"baz": "qux",
				"foo": "bar"
			}, [
				{"op": "remove", "path": "/baz"}
			]))
			.to.deep.equal({"foo": "bar"});
	});

	it('Removes an Array Element', function () {
		expect(JsonPatch.patch(
			{"foo": ["bar", "qux", "baz"]}, [
				{"op": "remove", "path": "/foo/1"}
			]))
			.to.deep.equal({"foo": ["bar", "baz"]});
	});

	it('Replacing a Value', function () {
		expect(JsonPatch.patch(
			{
				"baz": "qux",
				"foo": "bar"
			}, [
				{"op": "replace", "path": "/baz", "value": "boo"}
			]
		))
			.to.deep.equal({
				"baz": "boo",
				"foo": "bar"
			});
	});

	it('Moving a Value', function () {
		expect(JsonPatch.patch(
			{
				"foo": {
					"bar": "baz",
					"waldo": "fred"
				},
				"qux": {
					"corge": "grault"
				}
			}, [
				{"op": "move", "from": "/foo/waldo", "path": "/qux/thud"}
			]
		))
			.to.deep.equal({
				"foo": {
					"bar": "baz"
				},
				"qux": {
					"corge": "grault",
					"thud": "fred"
				}
			});
	});

	it('Moving a Value', function () {
		expect(JsonPatch.patch(
			{"foo": ["all", "grass", "cows", "eat"]}, [
				{"op": "move", "from": "/foo/1", "path": "/foo/3"}
			]
		))
			.to.deep.equal({"foo": ["all", "cows", "eat", "grass"]});
	});
});