'use strict';

var JsonFilter = require('../json-filter');
var expect = require('chai').expect;

describe("Json Filter ", function () {
	it('filters', function () {
		expect(JsonFilter.filter({"a": "b"}, "*")).to.deep.equal({"a": "b"});
		expect(JsonFilter.filter({"a": "b"}, {"b": "*"})).to.deep.equal({});
		expect(JsonFilter.filter({"a": "b"}, {"a": "*"})).to.deep.equal({"a": "b"});
		expect(JsonFilter.filter({"a": ["b"]}, {"a": "*"})).to.deep.equal({"a": ['b']});
		expect(JsonFilter.filter({"a": ["b"]}, {"a": {"b": "*"}})).to.deep.equal({});
		expect(JsonFilter.filter({"a": "b", "b": "c"}, {"a": "*"})).to.deep.equal({"a": "b"});
		expect(JsonFilter.filter({"a": {"b": "c", "d": "e"}}, {"a": "*"})).to.deep.equal({"a": {"b": "c", "d": "e"}});
		expect(JsonFilter.filter({"a": {"b": "c", "d": "e"}}, {"a": {"b": "*"}})).to.deep.equal({"a": {"b": "c"}});
		expect(JsonFilter.filter({
			"a": {"b": "c", "d": "e"},
			"f": "sdf"
		}, {"a": {"b": "*"}})).to.deep.equal({"a": {"b": "c"}});
		expect(JsonFilter.filter({"a": {"b": "c", "d": "e"}, "f": "sdf"}, {
			"a": {"b": "*"},
			"f": "*"
		})).to.deep.equal({"a": {"b": "c"}, "f": "sdf"});
		expect(JsonFilter.filter({}, "*")).to.deep.equal({});
		expect(JsonFilter.filter({}, {"a": "*"})).to.deep.equal({});
		expect(JsonFilter.filter({"a": "a"}, {})).to.deep.equal({});
		expect(JsonFilter.filter({"a": {b: 'b'}}, {a: {c: '*'}})).to.deep.equal({});
	});

	it('unions', function () {
		expect(JsonFilter.union([{"a": {}}, {"a": {}}])).to.deep.equal({"a": {}});
		expect(JsonFilter.union([{"a": {}, "b": {}}, {"a": {}}])).to.deep.equal({"a": {}, "b": {}});
		expect(JsonFilter.union([{"a": "*", "b": "*"}, {"a": "*", "c": "*"}])).to.deep.equal({"a": "*", "b": "*", "c": "*"});
		expect(JsonFilter.union([{"a": "*", "b": "*", "c": {"d": "*"}}, {"a": "*", "c": "*"}])).to.deep.equal({
			"a": "*",
			"b": "*",
			"c": "*"
		});
		expect(JsonFilter.union([{"a": "*", "b": {"c": "*", "d": "*"}}, {"a": "*", "c": "*"}])).to.deep.equal({
			a: "*",
			b: {c: "*", d: "*"},
			c: "*"
		});
		expect(JsonFilter.union([{"a": "*"}, "*"])).to.deep.equal("*");
	});

	it('performs pointer unions', function () {
		expect(JsonFilter.pointerUnion([])).to.deep.equal({});
		expect(JsonFilter.pointerUnion([''])).to.deep.equal('*');
		expect(JsonFilter.pointerUnion(['/'])).to.deep.equal({'': '*'});
		expect(JsonFilter.pointerUnion(['/a'])).to.deep.equal({a: '*'});
		expect(JsonFilter.pointerUnion(['/a/b'])).to.deep.equal({a: {b: '*'}});

		expect(JsonFilter.pointerUnion(['/a/b', '/c/d', '/e/f'])).to.deep.equal({a: {b: '*'}, c: {d: '*'}, e: {f: '*'}});
		expect(JsonFilter.pointerUnion(['/a/b', '/a/c', '/a/d'])).to.deep.equal({a: {b: '*', c: '*', d: '*'}});
		expect(JsonFilter.pointerUnion(['/a/b/c', '/a/d/e'])).to.deep.equal({a: {b: {c: '*'}, d: {e: '*'}}});

		expect(JsonFilter.pointerUnion(['/a/b/c', '/a/b'])).to.deep.equal({a: {b: '*'}});
		expect(JsonFilter.pointerUnion(['/a/b', '/a/b/c'])).to.deep.equal({a: {b: '*'}});
	});

	it('filter by value', function () {
		expect(JsonFilter.valueFilter({a: 'a', b: 'b'}, 'a')).to.deep.equal({a: 'a'});
		expect(JsonFilter.valueFilter({a: 'a', b: 'b', c: 'a'}, 'a')).to.deep.equal({a: 'a', c: 'a'});
		expect(JsonFilter.valueFilter({a: 'a', b: 'b', c: {d: 'a'}}, 'a')).to.deep.equal({a: 'a', c: {d: 'a'}});
		expect(JsonFilter.valueFilter({a: 'a', b: null}, null)).to.deep.equal({b: null});
		expect(JsonFilter.valueFilter({a: 'a', b: null}, 'a')).to.deep.equal({a: 'a'});
		expect(JsonFilter.valueFilter({a: {b: null, c: '*'}}, '*')).to.deep.equal({a: {c: '*'}});
		expect(JsonFilter.valueFilter({a: {b: null, c: '*'}}, 'a')).to.deep.equal({});
	});

	it('calculates set difference', function () {
		expect(JsonFilter.setDifference({}, "*")).to.deep.equal({});
		expect(JsonFilter.setDifference("*", {})).to.equal("*");

		expect(JsonFilter.setDifference({"a":"*"}, "*")).to.deep.equal({});
		expect(JsonFilter.setDifference({"a":"*"}, {})).to.deep.equal({"a":"*"});

		expect(JsonFilter.setDifference({"a":"*"}, {"a": "*"})).to.deep.equal({});
		expect(JsonFilter.setDifference({"a":"*"}, {"b": "*"})).to.deep.equal({"a":"*"});

		expect(JsonFilter.setDifference({"a": {"b": "*"}}, {"a": "*"})).to.deep.equal({});
		expect(JsonFilter.setDifference({"a": {"b": "*"}}, {"a": {"b": "*"}})).to.deep.equal({});
		expect(JsonFilter.setDifference({"a": {"b": "*"}}, {"a": {"c": "*"}})).to.deep.equal({"a": {"b": "*"}});
		expect(JsonFilter.setDifference({"a": {"b": "*", "c": "*"}}, {"a": {"c": "*"}})).to.deep.equal({"a": {"b": "*"}});


		// expect(JsonFilter.valueFilter({a: 'a', b: 'b', c: 'a'}, 'a')).to.deep.equal({a: 'a', c: 'a'});
		// expect(JsonFilter.valueFilter({a: 'a', b: 'b', c: {d: 'a'}}, 'a')).to.deep.equal({a: 'a', c: {d: 'a'}});
		// expect(JsonFilter.valueFilter({a: 'a', b: null}, null)).to.deep.equal({b: null});
		// expect(JsonFilter.valueFilter({a: 'a', b: null}, 'a')).to.deep.equal({a: 'a'});
		// expect(JsonFilter.valueFilter({a: {b: null, c: '*'}}, '*')).to.deep.equal({a: {c: '*'}});
		// expect(JsonFilter.valueFilter({a: {b: null, c: '*'}}, 'a')).to.deep.equal({});
	});

});