'use strict';

var JsonPointer = require('../json-pointer');
var expect = require('chai').expect;
// https://tools.ietf.org/html/rfc6901#page-2

describe("Json Pointer", function () {
	it("can do has", function () {
		expect(JsonPointer.has({"a": "b"}, "")).to.equal(true);
		expect(JsonPointer.has({"a": "b"}, "/")).to.equal(false);
		expect(JsonPointer.has({"": "b"}, "/")).to.equal(true);
		expect(JsonPointer.has({"a": "b"}, "/a")).to.equal(true);
		expect(JsonPointer.has({"a": "b"}, "/c")).to.equal(false);
		expect(JsonPointer.has({"a": "b"}, "/b/c")).to.equal(false);
		expect(JsonPointer.has({"a": {b: 'c'}}, "/a/b")).to.equal(true);
		expect(JsonPointer.has({"a": {b: 'c'}}, "/a/c")).to.equal(false);
	});

	it("can do get", function () {
		expect(JsonPointer.get({"a": "b"}, "")).to.deep.equal({"a": "b"});
		expect(JsonPointer.get({"a": "b"}, "/")).to.equal(undefined);
		expect(JsonPointer.get({"a": "b"}, "/a")).to.equal("b");
		expect(JsonPointer.get({"a": "b"}, "/c")).to.equal(undefined);
		expect(JsonPointer.get({"a": "b"}, "/b/c")).to.equal(undefined);
		expect(JsonPointer.get({"a": "b"}, "/c/d")).to.equal(undefined);
		expect(JsonPointer.get({"a": {b: 'c'}}, "/a/b")).to.equal('c');
	});

	it("can do set", function() {
		expect(JsonPointer.set(1, "", 'X')).to.deep.equal('X');
		expect(JsonPointer.set(1, "/a/b", 'X')).to.deep.equal({a: {b: 'X'}});
		expect(JsonPointer.set({}, "", 'X')).to.deep.equal('X');
		expect(JsonPointer.set({}, "/a/b", 'X')).to.deep.equal({a: {b: 'X'}});
		expect(JsonPointer.set({"a": "b"}, "", 'X')).to.deep.equal('X');
		expect(JsonPointer.set({"a": "b"}, "/", 'X')).to.deep.equal({a: "b", '': 'X'} );
		expect(JsonPointer.set({"a": "b"}, "/a", 'X')).to.deep.equal({a: "X"} );
		expect(JsonPointer.set({"a": "b"}, "/c", 'X')).to.deep.equal({a: "b", 'c': 'X'} );
		expect(JsonPointer.set({"a": "b"}, "/a/c", 'X')).to.deep.equal({a: {c: 'X'}} );
		expect(JsonPointer.set({"a": "b"}, "/c/d", 'X')).to.deep.equal({a: 'b', c: {d: 'X'}} );
		expect(JsonPointer.set({"a": {b: 'c'}}, "/a/b", 'X')).to.deep.equal({a: {b: 'X'}});
	});

	it('performs immutable set operations', function () {
		var testObject = {a: '0', b: {c: '1'}};
		var result = JsonPointer.set(testObject, '/b/d', '2');
		expect(testObject).to.not.equal(result);
		expect(testObject).to.deep.equal({a: '0', b: {c: '1'}});

		expect(result).to.deep.equal({a: '0', b: {c: '1', d: '2'}});
		expect(result.a).to.equal(testObject.a);
		expect(result.b).to.not.equal(testObject.b);
		expect(result.b.c).to.equal(testObject.b.c);
	});
});