'use strict';

var JsonMerge = require('../json-merge');
var expect = require('chai').expect;

describe("Json Merge ", function () {
	var testCases = [
		// from https://tools.ietf.org/html/rfc7396#appendix-A
		{"original": {"a": "b"}, "patch": {"a": "c"}, "result": {"a": "c"}},
		{"original": {"a": "b"}, "patch": {"b": "c"}, "result": {"a": "b", "b": "c"}},
		{"original": {"a": "b"}, "patch": {"a": null}, "result": {}},
		{"original": {"a": "b", "b": "c"}, "patch": {"a": null}, "result": {"b": "c"}},
		{"original": {"a": ["b"]}, "patch": {"a": "c"}, "result": {"a": "c"}},
		{"original": {"a": "c"}, "patch": {"a": ["b"]}, "result": {"a": ["b"]}},
		{"original": {"a": {"b": "c"}}, "patch": {"a": {"b": "d", "c": null}}, "result": {"a": {"b": "d"}}},
		{"original": {"a": [{"b": "c"}]}, "patch": {"a": [1]}, "result": {"a": [1]}},
		{"original": ["a", "b"], "patch": ["c", "d"], "result": ["c", "d"]},
		{"original": {"a": "b"}, "patch": ["c"], "result": ["c"]},
		{"original": {"a": "foo"}, "patch": null, "result": null},
		{"original": {"a": "foo"}, "patch": "bar", "result": "bar"},
		{"original": {"e": null}, "patch": {"a": 1}, "result": {"e": null, "a": 1}},
		{"original": [1, 2], "patch": {"a": "b", "c": null}, "result": {"a": "b"}},
		{"original": {}, "patch": {"a": {"bb": {"ccc": null}}}, "result": {"a": {"bb": {}}}}
	];

	function createMergeTest(testCase) {
		return function () {
			var actualResult = JsonMerge.merge(testCase.original, testCase.patch);
			expect(actualResult).to.deep.equal(testCase.result);
		}
	}

	for (var i = 0, testCase; testCase = testCases[i]; i++) {
		it("Passes test case " + i + " : " + JSON.stringify(testCase), createMergeTest(testCase));
	}

	it("Merge produces immutable results", function () {
		var original = {
			"b": {
				"c": "rabbit",
				"e": "horse"
			},
			"c": "bird",
			"f": "turkey",
			"g": "chipmunk",
			"z": {
				"x": "monkey"
			}
		};

		var patch = {
			"a": "cat",
			"b": {
				"c": "dog",
				"d": "mouse"
			},
			"f": null,
			"g": "emu",
			"y": {
				"h": "bear"
			}

		};

		var result = {
			"a": "cat",
			"b": {
				"c": "dog",
				"d": "mouse",
				"e": "horse"
			},
			"c": "bird",
			"g": "emu",
			"z": {
				"x": "monkey"
			},
			"y": {
				"h": "bear"
			}
		};

		var actualResult = JsonMerge.merge(original, patch);

		expect(actualResult).to.deep.equal(result);

		expect(actualResult).to.not.equal(original);
		expect(actualResult.b).to.not.equal(original.b);
		expect(actualResult.b).to.not.equal(original.b);
		expect(actualResult.b).to.not.equal(original.b);
		expect(actualResult.y).to.not.equal(patch.y);

		expect(actualResult.z).to.equal(original.z);
	});

	it("Merge applies no change if no value equals old value", function () {
		var original = {
			"a": "a"
		};

		var patch = {
			"a": "a"
		};

		var result = {
			"a": "a"
		};

		var actualResult = JsonMerge.merge(original, patch);

		expect(actualResult).to.equal(original);
	});

	function createGeneratePatchTest(testCase) {
		return function () {
			var actualResult = JsonMerge.generatePatch(testCase.original, testCase.result);
			expect(actualResult).to.deep.equal(testCase.patch);
		}
	}


	var patchGenerationTestCases = [
		// from https://tools.ietf.org/html/rfc7396#appendix-A
		{"original": {"a": "b"}, "patch": {"a": "c"}, "result": {"a": "c"}},                                    // 0
		{"original": {"a": "b"}, "patch": {"b": "c"}, "result": {"a": "b", "b": "c"}},                          // 1
		{"original": {"a": "b"}, "patch": {"a": null}, "result": {}},                                           // 2
		{"original": {"a": "b", "b": "c"}, "patch": {"a": null}, "result": {"b": "c"}},                         // 3
		{"original": {"a": ["b"]}, "patch": {"a": "c"}, "result": {"a": "c"}},                                  // 4
		{"original": {"a": "c"}, "patch": {"a": ["b"]}, "result": {"a": ["b"]}},                                // 5
		//{"original": {"a": {"b": "c"}}, "patch": {"a": {"b": "d", "c": null}}, "result": {"a": {"b": "d"}}},
		{"original": {"a": [{"b": "c"}]}, "patch": {"a": [1]}, "result": {"a": [1]}},
		{"original": ["a", "b"], "patch": ["c", "d"], "result": ["c", "d"]},
		{"original": {"a": "b"}, "patch": ["c"], "result": ["c"]},
		{"original": {"a": "foo"}, "patch": null, "result": null},
		{"original": {"a": "foo"}, "patch": "bar", "result": "bar"},
		{"original": {"e": null}, "patch": {"a": 1}, "result": {"e": null, "a": 1}},
		//{"original": [1, 2], "patch": {"a": "b", "c": null}, "result": {"a": "b"}},
		//{"original": {}, "patch": {"a": {"bb": {"ccc": null}}}, "result": {"a": {"bb": {}}}}
		{"original": {"a": {"d": "d"}}, "patch": {"b": {}}, "result": {"a": {"d": "d"}, "b": {}}},
		{"original": {"a": {"d": "d"}}, "patch": {"b": "c"}, "result": {"a": {"d": "d"}, "b": "c"}},
		{"original": {"a": "b"}, "patch": {}, "result": {"a": "b"}}
	];

	for (var i = 0, testCase; testCase = patchGenerationTestCases[i]; i++) {
		it("Passes generate patch test case " + i + " : " + JSON.stringify(testCase), createGeneratePatchTest(testCase));
	}
});