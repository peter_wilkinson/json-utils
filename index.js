'use strict';

module.exports = {
	pointer: require('./json-pointer'),
	filter: require('./json-filter'),
	merge: require('./json-merge'),
	patch: require('./json-patch')
};