'use strict';

var JsonPointer = require('./json-pointer');

var OBJECT_CONSTRUCTOR = {}.constructor;

function applyFilter(original, filter) {
	if (!filter) {
		return {};
	}

	if (filter === "*" || original === undefined || original === null) {
		return original;
	}

	var filtered = {};
	for (var i = 0, keys = Object.keys(filter), len = keys.length; i < len; i++) {
		var filterKey = keys[i];
		if (original.hasOwnProperty(filterKey)) {
			var filterValue = filter[filterKey];
			var originalValue = original[filterKey];
			if (!originalValue || originalValue.constructor !== OBJECT_CONSTRUCTOR) {
				if (filterValue === "*") {
					filtered[filterKey] = originalValue;
				}
			}
			else {
				var filteredValue = applyFilter(originalValue, filterValue);
				for (var filteredKey in filteredValue) {
					filtered[filterKey] = filteredValue;
					break;
				}
			}
		}
	}

	return filtered;
}

function union(filters) {
	if (filters.length === 0) {
		return undefined;
	}
	if (filters.length === 1) {
		return filters[0];
	}

	var filterResult = {};
	for (var i = 0, iLen = filters.length; i < iLen; i++) {
		var filter = filters[i];
		if (filters[i] === '*') {
			return '*';
		}
		for (var j = 0, keys = Object.keys(filter), jLen = keys.length; j < jLen; j++) {
			var filterKey = keys[j];
			if (filterResult[filterKey] === undefined) {
				var unionFilters = [];
				for (var k = i, kLen = filters.length; k < kLen; k++) {
					var filterTwo = filters[k];
					if (filterTwo[filterKey] !== undefined) {
						unionFilters.push(filterTwo[filterKey]);
					}
				}
				filterResult[filterKey] = union(unionFilters);
			}
		}
	}

	return filterResult;
}

function pointerUnion(pointers) {
	var filter = {};
	for (var i = 0, pointerCount = pointers.length; i < pointerCount; i++) {
		var pointerPath = pointers[i];
		if (pointerPath === "*") {
			return {};
		}
		if (JsonPointer.has(filter, pointerPath)) {
			filter = JsonPointer.set(filter, pointerPath, "*");
		}
		else {
			// check if ancestor is '*'
			var pointerTokens = pointerPath.split('/');
			var currentLocation = filter;
			for (var j = 1, jLen = pointerTokens.length; j < jLen; j++) {
				currentLocation = currentLocation[pointerTokens[j]];
				//noinspection FallThroughInSwitchStatementJS
				switch (currentLocation) {
					case undefined:
						filter = JsonPointer.set(filter, pointerPath, "*");
					case '*':
						j = jLen;
						break;
				}
			}
		}
	}

	return filter;
}

function valueFilter(object, value) {
	if (object === value) {
		return object;
	}
	if (object === undefined || object === null || object.constructor !== OBJECT_CONSTRUCTOR) {
		return null;
	}
	var result = {};
	for (var key in object) {
		var propertyValue = object[key];
		if (value === propertyValue) {
			result[key] = propertyValue;
		}
		else {
			var filteredValue = valueFilter(propertyValue, value);
			if (filteredValue !== null) {
				for (var filteredKey in filteredValue) {
					result[key] = filteredValue;
					break;
				}
			}
		}
	}
	return result;
}

function setDifference(filterOne, filterTwo) {
    if (filterTwo === "*") {
        return {};
    }
    if (filterOne === "*") {
        return filterOne;
    }

    if (Object.keys(filterTwo).length === 0) {
        return filterOne;
    }

    let diff = {};
    let keys = Object.keys(filterOne);

    for (let i=0; i < keys.length; i++) {
        let key = keys[i];
        let valueDiff = setDifference(filterOne[key], filterTwo[key]);
        if (Object.keys(valueDiff).length !== 0) {
            diff[key] = valueDiff;
        }
    }
    return diff;
}

module.exports = {
	"filter": applyFilter,
	"union": union,
	"pointerUnion": pointerUnion,
	"valueFilter": valueFilter,
	"setDifference": setDifference
};